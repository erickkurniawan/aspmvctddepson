﻿using ASPTDD.Models;
using ASPTDD.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASPTDD.Controllers
{
    public class MovieController : Controller
    {
        private IMovieRepository _movieRepository;
        public MovieController(IMovieRepository movieRepository)
        {
            _movieRepository = movieRepository;
        }

        public MovieController()
        {

        }

        // GET: Movie
        public ActionResult Index()
        {
            var model = _movieRepository.GetAll();
            return View(model);
        }
    }
}