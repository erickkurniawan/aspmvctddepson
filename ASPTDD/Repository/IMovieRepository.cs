﻿using ASPTDD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASPTDD.Repository
{
    public interface IMovieRepository
    {
        IEnumerable<Movie> GetAll();
    }
}