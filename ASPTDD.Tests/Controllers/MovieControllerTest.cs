﻿using ASPTDD.Controllers;
using ASPTDD.Models;
using ASPTDD.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;


namespace ASPTDD.Tests.Controllers
{
   
    [TestClass]
    public class MovieControllerTest
    {
        [TestMethod]
        public void memastikan_view_default_dipanggil()
        {
            var controller = new MovieController();
            var result = controller.Index() as ViewResult;
            Assert.AreEqual("", result.ViewName);
        }

        [TestMethod]
        public void memastikan_controller_harus_balikin_model_movies()
        {
            var movies = new List<Movie>()
            {
                new Movie {Title="Tomb Raider"},
                new Movie {Title="Black Panther"}
            };

            var repository = new Mock<IMovieRepository>();
            var controller = new MovieController(repository.Object);
            repository.Setup(m => m.GetAll()).Returns(movies);

            var result = controller.Index() as ViewResult;
            var model = result.ViewData.Model as List<Movie>;
            Assert.IsTrue(movies.SequenceEqual(model));
        }
    }

    
}
